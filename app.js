const boton = document.querySelector('#boton');
const h2 = document.querySelector('h2');

let counter = 0;

boton.addEventListener('click', function() {

    if ( counter === 10 ) {
        this.classList.add("restar");
        this.classList.remove("sumar");
    }
    if ( counter === 0 ) {
        this.classList.add("sumar");
        this.classList.remove("restar");
    }

    const clase = boton.className;

    switch ( clase) {
        case 'sumar':
            counter++;
            h2.textContent = counter;
            break;
        case 'restar':
            counter--;
            h2.textContent = counter;
        break;
        default:
            break;
    }
});



